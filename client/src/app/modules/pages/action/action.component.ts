import { Component, OnInit } from '@angular/core';
import {ActionPage} from '../../admin/types/actionpage';
import {Observable} from 'rxjs/Observable';
import {ActivatedRoute, ParamMap, Router} from '@angular/router';
import {ActionPageService} from '../../admin/services/actionpage.service';
import {AdminSandbox} from '../../admin/admin.sandbox';
import {DomSanitizer} from '@angular/platform-browser';

@Component({
  selector: 'app-action',
  templateUrl: './action.component.html',
  styleUrls: ['./action.component.scss']
})
export class ActionComponent implements OnInit {

  actionPage$: Observable<ActionPage>;
  errorMessage$: Observable<string>;

  constructor(
    private router: Router,
    private adminSandbox: AdminSandbox,
    private route: ActivatedRoute,
    private  domSanitizer: DomSanitizer
  ) { }

  ngOnInit(): void {
    this.actionPage$ = this.route.params.switchMap((params: ParamMap) => {
      if(params['name']) {
        return this.adminSandbox.getSingleActionPage(params['name']);
      }
      return Observable.throw(new Error('action page name was expected but none specified'));
    })
  }

  public _sanitize(string){
    return this.domSanitizer.bypassSecurityTrustResourceUrl('data:image/jpg;base64,' + string);
  }

}
