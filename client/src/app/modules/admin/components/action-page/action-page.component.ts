import {Component, Input, OnChanges, OnInit} from '@angular/core';
import {ActionPage} from '../../types/actionpage';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Observable} from 'rxjs/Observable';
import {Subject} from 'rxjs/Subject';
import {AdminSandbox} from '../../admin.sandbox';
import {Router} from '@angular/router';

@Component({
  selector: 'app-action-page',
  templateUrl: './action-page.component.html',
  styleUrls: ['./action-page.component.scss']
})
export class ActionPageComponent implements OnInit, OnChanges {

  @Input() public actionPage: ActionPage;
  public actionPageFormGroup: FormGroup;
  errorMessage$: Observable<string>;
  submit$: Subject<ActionPage>;

  constructor(private adminSandBox: AdminSandbox, private router: Router) {
    this.submit$ = new Subject();
    this.actionPageFormGroup = this.createActionFormGroup();
  }

  ngOnInit() {
    this.errorMessage$ = this.submit$
      .mergeMap(actionPage => {
        if (!actionPage._id) {
          return this.adminSandBox.createActionPage(actionPage);
        } else {
          delete actionPage._id;
          return this.adminSandBox.updateActionPage(actionPage);
        }
      }).mergeMap(actionPage => {
        return this.router.navigate(['actiepagina/', actionPage.name]);
      }).map(navigationResult => {
        if (navigationResult) {
          return '';
          } else {
          return 'Something went wrong while saving the page, please check all values below and try to save again';
        }
      });
  }

  ngOnChanges() {
    this.actionPageFormGroup.reset({
      _id: this.actionPage._id,
      name: this.actionPage.name,
      title: this.actionPage.title,
      foundationInfo: this.actionPage.foundationInfo
    });
  }

  private createActionFormGroup(): FormGroup {
    return new FormGroup({
      _id: new FormControl(''),
      name: new FormControl('', [Validators.required, Validators.minLength(6)]),
      title: new FormControl('', [Validators.required, Validators.minLength(15)]),
      foundationInfo: new FormControl(''),
    });
  }
  get _id() {
    return this.actionPageFormGroup.get('_id');
  }
  get name() {
    return this.actionPageFormGroup.get('name');
  }
  get title() {
    return this.actionPageFormGroup.get('title');
  }
  get foundationInfo() {
    return this.actionPageFormGroup.get('foundationInfo');
  }
}
