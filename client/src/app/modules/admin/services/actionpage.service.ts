import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {ActionPage} from '../types/actionpage';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class ActionPageService {
  private actionPageUrl = 'api/dynamicactionpage';
  constructor(private _http: HttpClient) { }

  getActionPages(): Observable<ActionPage[]> {
    return this._http.get<ActionPage[]>(this.actionPageUrl);
  }

  getSingleActionPage(name: string): Observable<ActionPage> {
    return this._http.get<ActionPage>(`${this.actionPageUrl}/${name}`);
  }

  createActionPage(body): Observable<ActionPage> {
    return this._http.post<ActionPage>(this.actionPageUrl, body);
  }

  uploadImages(formData, headers) {
    return this._http.post(this.actionPageUrl +'/imageUpload', formData, {headers: headers, responseType: 'blob'}).subscribe(data => {
      console.log('upload success', data)
    });
  }

  updateActionPage(body): Observable<ActionPage> {
    return this._http.put<ActionPage>(this.actionPageUrl, body);
  }
}
