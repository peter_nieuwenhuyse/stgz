import { TestBed, inject } from '@angular/core/testing';

import { ActionpageService } from './actionpage.service';

describe('ActionpageService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ActionpageService]
    });
  });

  it('should be created', inject([ActionpageService], (service: ActionpageService) => {
    expect(service).toBeTruthy();
  }));
});
