export interface ActionPage {
  _id?: string;
  name: string;
  title: string;
  foundationInfo: string;
}

