import { Component, OnInit } from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {ActionPage} from '../../types/actionpage';
import {AdminSandbox} from '../../admin.sandbox';
import {ActivatedRoute, Router} from '@angular/router';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/observable/of';

@Component({
  selector: 'app-action-page-container',
  templateUrl: './action-page-container.component.html',
  styleUrls: ['./action-page-container.component.scss']
})
export class ActionPageContainerComponent implements OnInit {
  actionPage$: Observable<ActionPage>;
  constructor(private adminSandBox: AdminSandbox, private route: ActivatedRoute) {}

  ngOnInit() {
    this.actionPage$ = this.route.params.switchMap(params => {
      const name = params['name'] as string | undefined | null;
      if (name) {
        return this.adminSandBox.getSingleActionPage(name);
      } else {
        return Observable.of({
          id: '',
          name: '',
          title: '',
          foundationInfo: '',
        });
      }
    });
  }
}
