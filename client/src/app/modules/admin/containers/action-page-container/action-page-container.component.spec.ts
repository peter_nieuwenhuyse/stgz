import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActionPageContainerComponent } from './action-page-container.component';

describe('ActionPageContainerComponent', () => {
  let component: ActionPageContainerComponent;
  let fixture: ComponentFixture<ActionPageContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActionPageContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActionPageContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
