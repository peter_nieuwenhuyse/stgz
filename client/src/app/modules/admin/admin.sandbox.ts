import {Injectable} from '@angular/core';
import {ActionPageService} from './services/actionpage.service';
import {ActionPage} from './types/actionpage';
import {Observable} from 'rxjs/Observable';
import {HttpHeaders} from '@angular/common/http';

@Injectable()
export class AdminSandbox {
  constructor(
    // add all adminServices
    private actionPage: ActionPageService
  ) {}

  getSingleActionPage(name): Observable<ActionPage> {
    return this.actionPage.getSingleActionPage(name);
  }

  createActionPage(body): Observable<ActionPage> {
    return this.actionPage.createActionPage(body);
  }

  updateActionPage(body): Observable<ActionPage> {
    return this.actionPage.updateActionPage(body);
  }

  uploadFile(formData) {
    const headers = new HttpHeaders({})
     return this.actionPage.uploadImages(formData, headers)
  }
}
