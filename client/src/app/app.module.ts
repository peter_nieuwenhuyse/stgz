import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { RouterModule } from '@angular/router';
import { NavComponent } from '../common/nav/nav.component';
import { FooterComponent } from '../common/footer/footer.component';
import { HeaderComponent } from '../common/header/header.component';
import { FroalaEditorModule, FroalaViewModule } from 'angular-froala-wysiwyg';
import { ActionComponent } from './modules/pages/action/action.component';
import { ActionPageContainerComponent } from './modules/admin/containers/action-page-container/action-page-container.component';
import { ActionPageComponent } from './modules/admin/components/action-page/action-page.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { ActionPageService } from './modules/admin/services/actionpage.service';
import { AdminSandbox } from './modules/admin/admin.sandbox';
import {FileDropModule} from 'ngx-file-drop';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {
  ErrorStateMatcher,
  MatButtonModule, MatCardModule, MatDatepickerModule, MatFormFieldModule, MatIconModule, MatInputModule, MatMenuModule,
  MatNativeDateModule, MatOptionModule, MatRadioModule, MatSelectModule, MatSlideToggleModule,
  MatToolbarModule, ShowOnDirtyErrorStateMatcher
} from '@angular/material';
import { CloudinaryModule } from '@cloudinary/angular-5.x';
import * as  Cloudinary from 'cloudinary-core';


@NgModule({
  declarations: [
    AppComponent,
    ActionComponent,
    NavComponent,
    FooterComponent,
    HeaderComponent,
    ActionPageContainerComponent,
    ActionPageComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    FileDropModule,
    MatButtonModule,
    MatMenuModule,
    MatToolbarModule,
    MatIconModule,
    MatCardModule,
    BrowserAnimationsModule,
    MatFormFieldModule,
    MatInputModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatRadioModule,
    MatSelectModule,
    MatOptionModule,
    MatSlideToggleModule,
    CloudinaryModule.forRoot(Cloudinary, {cloud_name: 'www-stgz-nl', upload_preset: 'mypreset', private_cdn: 'true', cname:'stgz.images.com'}),
    FroalaEditorModule.forRoot(),
    FroalaViewModule.forRoot(),
    RouterModule.forRoot([
      {path: 'admin/createactionpage', component: ActionPageContainerComponent},
      {path: 'actiepagina/:name', component: ActionComponent},
      {path: '', component: ActionComponent}
    ])
  ],
  exports:[
    MatButtonModule,
    MatMenuModule,
    MatToolbarModule,
    MatIconModule,
    MatCardModule,
    BrowserAnimationsModule,
    MatFormFieldModule,
    MatInputModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatRadioModule,
    MatSelectModule,
    MatOptionModule,
    MatSlideToggleModule
  ],
  providers: [
    ActionPageService,
    AdminSandbox,
    {provide: ErrorStateMatcher, useClass: ShowOnDirtyErrorStateMatcher }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
