import {Connection} from 'mongoose';
import {BuddySchema} from './schemas/buddy.schema';

export const buddyProviders = [
    {
        provide: 'BuddyToken',
        useFactory: (connection: Connection) => connection.model('Buddy', BuddySchema),
        inject: ['DBConnectionToken'],
    }
]