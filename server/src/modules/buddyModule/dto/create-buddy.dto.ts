export class CreateBuddyDto {
    readonly name: string;
    readonly mail: string;
    readonly tel: string;
    readonly avatar: string;
}