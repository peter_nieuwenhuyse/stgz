import * as mongoose from 'mongoose';

export const BuddySchema = new mongoose.Schema(
    {
        name: String,
        mail: String,
        tel: String,
        avatar: String
    }
)