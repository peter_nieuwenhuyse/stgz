import {Component, Inject} from '@nestjs/common';
import {Buddy} from './interfaces/buddy.interface';
import {Model} from 'mongoose';
import {CreateBuddyDto} from './dto/create-buddy.dto';

@Component()
export class BuddyService {
    constructor(
        @Inject('BuddyToken') private readonly buddyModel: Model<Buddy>
    ) {}
    async create(createBuddyDto: CreateBuddyDto): Promise<Buddy> {
        const createdBuddy = new this.buddyModel(createBuddyDto);
        return await createdBuddy.save()
    }
    async findAll(): Promise<Buddy[]> {
        return await this.buddyModel.find().exec()
    }

    async findByMail(mail): Promise<Buddy> {
        return await this.buddyModel.findOne({'mail': mail}).exec()
    }

    async findById(id): Promise<Buddy> {
        return await this.buddyModel.findOne({'_id': id}).exec()
    }
}