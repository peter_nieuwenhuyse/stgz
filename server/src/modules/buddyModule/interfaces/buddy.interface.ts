import { Document } from 'mongoose'

export interface Buddy extends Document {
    readonly _id: string;
    readonly name: string;
    readonly mail: string;
    readonly tel: string;
    readonly avatar: string;
}
