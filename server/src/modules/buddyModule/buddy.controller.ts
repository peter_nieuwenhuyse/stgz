import {Body, Controller, Get, Param, Post} from '@nestjs/common';
import {BUDDY} from '../../routes';
import {Buddy} from './interfaces/buddy.interface';
import {BuddyService} from './buddy.service';
import {CreateBuddyDto} from './dto/create-buddy.dto';


@Controller(BUDDY)
export class buddyController {
    constructor(private readonly buddyService: BuddyService){}
    @Get()
    async findAll(): Promise<Buddy[]> {
        return this.buddyService.findAll();
    }

    @Get(':mail')
    async finOneByMail(@Param() params): Promise<Buddy> {
        return this.buddyService.findByMail(params.mail)
    }

    @Get(':_id')
    async findOneById(@Param() params): Promise<Buddy> {
        return this.buddyService.findById(params._id)
    }

    @Post()
    async create(@Body() createBuddyDto: CreateBuddyDto) {
        return this.buddyService.create(createBuddyDto);
    }
}