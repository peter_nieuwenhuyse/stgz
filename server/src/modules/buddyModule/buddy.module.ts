import {Module} from '@nestjs/common';
import {DatabaseModule} from '../databaseConnectionModule/database.module';
import {buddyController} from './buddy.controller';
import {BuddyService} from './buddy.service';
import {buddyProviders} from './buddy.providers';

@Module({
    imports: [DatabaseModule],
    controllers: [buddyController],
    components: [
        BuddyService,
        ...buddyProviders
    ],
})

export class BuddyModule {}