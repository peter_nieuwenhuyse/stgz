import {Body, Controller, Get, Param, Post, Put, Req} from '@nestjs/common';
import {DynamicActionPageService} from './dynamicActionPage.service';
import {DynamicActionPage} from './interfaces/dynamicActionPage.interface';
import {CreateDynmaicActionPageDto} from './dto/create-dynamicActionPage.dto';
import {ROUTE_DYNAMICACTIONPAGE} from '../../routes';

@Controller(ROUTE_DYNAMICACTIONPAGE)
export class DynamicActionPageController {
    constructor(private readonly dynamicActionPageService: DynamicActionPageService){}
    @Get()
    async findAll(): Promise<DynamicActionPage[]> {
        return this.dynamicActionPageService.findAll();
    }
    @Get(':name')
    async findOne(@Param() params): Promise<DynamicActionPage> {
        return this.dynamicActionPageService.findOne(params.name)
    }
    @Post()
    async create(@Body() createDynamicActionPageDto: CreateDynmaicActionPageDto) {
       return this.dynamicActionPageService.create(createDynamicActionPageDto);
    }
    @Put()
    async update(@Body() updateDynamicActionPageDto: CreateDynmaicActionPageDto) {
        return this.dynamicActionPageService.update(updateDynamicActionPageDto)
    }
}
