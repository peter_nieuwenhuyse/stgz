import {Module} from '@nestjs/common';
import {DatabaseModule} from '../databaseConnectionModule/database.module';
import {DynamicActionPageController} from './dynamicActionPage.controller';
import {DynamicActionPageService} from './dynamicActionPage.service';
import {dynamicActionPageProviders} from './dynamicActionPage.providers';

@Module({
    imports: [DatabaseModule],
    controllers: [ DynamicActionPageController],
    components: [
        DynamicActionPageService,
        ...dynamicActionPageProviders
    ],
})
export class DynamicActionPageModule {}