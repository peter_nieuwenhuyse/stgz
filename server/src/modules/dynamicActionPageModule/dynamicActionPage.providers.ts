import { Connection } from 'mongoose';
import { DynamicActionPageSchema } from './schemas/dynamicActionPage.schema';

export const dynamicActionPageProviders = [
    {
        provide: 'DynamicActionPageToken',
        useFactory: (connection: Connection) => connection.model('DynamicActionPage', DynamicActionPageSchema),
        inject: ['DBConnectionToken'],
    }
];