import { Model } from 'mongoose';
import { Component, Inject } from '@nestjs/common';
import { DynamicActionPage } from './interfaces/dynamicActionPage.interface';
import { CreateDynmaicActionPageDto} from './dto/create-dynamicActionPage.dto';

@Component()
export class DynamicActionPageService {
    constructor(
        @Inject('DynamicActionPageToken') private readonly dynamicActionPageModel: Model<DynamicActionPage>
    ) {}

    async create(createDynamicActionPageDto: CreateDynmaicActionPageDto): Promise<DynamicActionPage> {
        const createdDynamacActionPage = new this.dynamicActionPageModel(createDynamicActionPageDto);
        return await createdDynamacActionPage.save();
    }

    async findAll(): Promise<DynamicActionPage[]> {
        return await this.dynamicActionPageModel.find().exec()
    }

    async findOne(name): Promise<DynamicActionPage> {
        return await this.dynamicActionPageModel.findOne({'name': name}).exec()
    }

    async update(updateDynamicActionPageDto: CreateDynmaicActionPageDto) : Promise<DynamicActionPage> {
        const id = updateDynamicActionPageDto.id;
        return await this.dynamicActionPageModel.findByIdAndUpdate(id, {$set: updateDynamicActionPageDto}, {new: true}).exec()
    }

}