import { Document } from 'mongoose';

export interface DynamicActionPage extends Document {
    readonly id: string;
    readonly name: string;
    readonly title: string;
    readonly foundationInfo : string;
    readonly sliderImages: [
        {
            name: string;
            imagePath: string;
            value: string;
        }
        ]
}