import * as mongoose from 'mongoose';

export const DynamicActionPageSchema = new mongoose.Schema(
    {
        id: String,
        name: String, // used for navigation in the url
        title: String,
        foundationInfo : String,
        sliderImages: [
            {
                name: String,
                imagePath: String,
                value: String
            }
        ]
    }
);