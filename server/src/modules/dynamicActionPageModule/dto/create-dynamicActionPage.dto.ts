export class CreateDynmaicActionPageDto {
    readonly id: string;
    readonly name: string;
    readonly title: string;
    readonly foundationInfo : string;
    readonly sliderImages : [{
        name: string;
        imagePath: string;
        value: string;
    }]
}