import { Document } from 'mongoose';

export interface UserUnknown extends Document {
    readonly ip : any,
    readonly pageViews: [PageView]
}

export interface UserKnown extends Document {
    readonly ip :any,
    readonly pageViews: [PageView],
    readonly api_key: string,
}

export interface KnownUser extends Document {
    readonly mail: string,
    readonly api_key: string
}

export interface PageView extends Document {
    readonly pageView: string,
    readonly date: string
}