import {Component, Inject} from '@nestjs/common';
import {Model} from 'mongoose';
import {KnownUser, UserKnown, UserUnknown} from './interfaces/pageviews.interface';
import {CreateUserUnknownDto} from './dto/create-UserUnknown.dto';
import {CreateUserKnownDto} from './dto/create-UserKnown.dto';

@Component()
export class ExternalService {
    constructor(
        @Inject('KnownUserToken') private readonly knownUserModel: Model<KnownUser>,
        @Inject('UserKnownToken') private readonly userKnownModel: Model<UserKnown>,
        @Inject('UserUnknowToken') private readonly userUnknownModel: Model<UserUnknown>
    ) {}

    async create(body){
        if(!body.api_key && !body.mail){
            return this.shouldcreateOrUpdateUnknownUser(body)
        } else if(!body._api_key && body.mail) {
            return this.checkIfUserHasApikeyOrCreateOne(body)
        } else if (body.api_key) {
            return this.userKnownModel.update({'api_key': body.api_key}, {$push:{'pageViews': body.pageView}})
        }
    }

    async shouldcreateOrUpdateUnknownUser(createUserUnknow: CreateUserUnknownDto): Promise<UserUnknown> {
        //check if we had previous pageviews
        const alreadyExists = this.userUnknownModel.find({'ip':createUserUnknow.ip}).count();
        if(alreadyExists) {
            //this ip address was already active on the site
          return await this.userUnknownModel.update({'ip': createUserUnknow.ip}, {$push:{'pageViews':createUserUnknow.pageView}})
        } else {
            // new ip address
            return await this.userUnknownModel.create(createUserUnknow);
        }
    }

    async checkIfUserHasApikeyOrCreateOne(body: CreateUserKnownDto):Promise<UserKnown> {
        const hasApiKey = this.knownUserModel.findOne({'mail':body.mail});
        if (hasApiKey) {
            return await this.userKnownModel.update({'mail': body.mail}, {$push:{'pageViews': body.pageView}})
        } else {
            const newApiKey = Math.floor((Math.random() * 10000000000) +1);
            const createObject = body;
            createObject.api_key = newApiKey;
            await this.knownUserModel.create({'mail': createObject.mail, 'api_key': createObject.api_key}).then(_ => {
                return this.userKnownModel.create(createObject);
            })
        }
    }
}