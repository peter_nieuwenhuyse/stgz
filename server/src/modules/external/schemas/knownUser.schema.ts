import * as mongoose from 'mongoose';

export const KnownUserSchema = new mongoose.Schema({
    mail: String,
    api_key: String
});