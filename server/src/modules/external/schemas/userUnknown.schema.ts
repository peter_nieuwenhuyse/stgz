import * as mongoose from 'mongoose';

export const UserUnknownSchema = new mongoose.Schema({
    ip: String,
    pageViews: [
        {
            pageView: String,
            date: String
        }
    ]
})