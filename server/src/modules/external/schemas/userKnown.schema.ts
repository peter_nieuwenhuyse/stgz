import * as mongoose from 'mongoose';

export const UserKnownSchema = new mongoose.Schema({
    ip: String,
    pageViews: [
        {
            pageView: String,
            date: String,
        }
    ],
    api_key: String
});