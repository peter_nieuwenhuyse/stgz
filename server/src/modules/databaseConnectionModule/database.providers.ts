import * as mongoose from 'mongoose'
export const databaseProviders = [
    {
        provide: 'DBConnectionToken',
        useFactory: async () => {
            (mongoose as any).Promise = global.Promise;
            return await mongoose.connect('mongodb://stgz:secret@ds121289.mlab.com:21289/stgz')
        }
    }
];