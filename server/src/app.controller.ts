import {Get, Controller, Res} from '@nestjs/common';
import * as path from 'path';
import {ROUTE_TEST} from './routes';

@Controller()
export class AppController {
	@Get()
	root(@Res() response): void {
	    response.sendFile(path.resolve('../dist/index.html'));
    }

    @Get(ROUTE_TEST)
    testApi(): string {
	    return 'my great api'
    }
}
