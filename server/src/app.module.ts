import {MiddlewaresConsumer, Module, NestModule, RequestMethod} from '@nestjs/common';
import { AppController } from './app.controller';
import {FrontendMiddleware} from './frontend.middleware';
import {DynamicActionPageModule} from './modules/dynamicActionPageModule/dynamicActionPage.module';
import {BuddyModule} from './modules/buddyModule/buddy.module';
import {AuthModule} from './modules/authModule/auth.module';

@Module({
  imports: [
      DynamicActionPageModule,
      BuddyModule,
      AuthModule
  ],
  controllers: [AppController],
  components: [],
})
export class ApplicationModule implements NestModule {
  configure(consumer: MiddlewaresConsumer): void {
    consumer.apply(FrontendMiddleware).forRoutes(
        {
            path: '/**',
            method: RequestMethod.ALL,
        },
    );
  }
}
