import { NestFactory } from '@nestjs/core';
import { ApplicationModule } from './app.module';
const express = require('express');

async function bootstrap() {
	const app = await NestFactory.create(ApplicationModule);
	app.use(express.json({limit: '50mb'}));
    app.use(express.urlencoded({limit: '50mb'}));
	await app.listen(3000);
}
bootstrap();
